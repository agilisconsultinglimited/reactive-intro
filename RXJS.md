# Reactive Programming

## Sketch
Intro from the below 2-minutes
Explain immutable, examples
What's the point? Teach by example:
Because data or events are now an immutable array, we can do the usual array things with them - filter, map etc.
- throttle mouseenter by time. Show how to do it with timers, then observable alternative with `debounce`
- filter mouse moves to only react on vertical moves, and only when bigger than 5px: `debounce`, `distinct`
- get notified of log data, but in chunks: `bufferWithCountOrTime`

## References
https://medium.com/@andrestaltz/2-minute-introduction-to-rx-24c8ca793877
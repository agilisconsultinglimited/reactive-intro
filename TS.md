# TypeScript

## String value enforcement
```
type Size = 'xs' | 's' | 'm' | 'l' | 'xl';

interface Product {
    size: Size
}

function isSize(size: Size, value: Size): Boolean {
  return size === value
}

const size : Size = 'm'
const notSize = 'abc'

isSize(size, 'xs')
isSize(size, 'abc')
isSize(notSize, 'm')
```